
set(ARDUINO_INCLUDE_DIR  "D:/Program Files (x86)/Arduino/hardware/arduino/avr/cores/arduino" )
set (ARDUINO_STD_LIB "D:/Program Files (x86)/Arduino/libraries" )
set (ARDUINO_EXT_LIB "E:/Documents/Arduino/libraries" )
include_directories(${ARDUINO_INCLUDE_DIR})
#link_directories(${ARDUINO_INCLUDE_DIR})

link_libraries(arduino)
list(APPEND LIB_SRC
        ${ARDUINO_INCLUDE_DIR}/HardwareSerial.cpp
        ${ARDUINO_INCLUDE_DIR}/HardwareSerial0.cpp
        ${ARDUINO_INCLUDE_DIR}/HardwareSerial1.cpp
        ${ARDUINO_INCLUDE_DIR}/HardwareSerial2.cpp
        ${ARDUINO_INCLUDE_DIR}/HardwareSerial3.cpp
        )
list(APPEND LIB_HEADERS
        ${ARDUINO_INCLUDE_DIR}/HardwareSerial.h
        ${ARDUINO_INCLUDE_DIR}/HardwareSerial_private.h

        )

include_directories(libraries/Wire)
include_directories(${ARDUINO_EXT_LIB}/NewPing/src)
include_directories( ${ARDUINO_STD_LIB}/servo/src)
include_directories(libraries/Adafruit_MCP4725)