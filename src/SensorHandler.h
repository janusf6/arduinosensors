//
// Created by Janus on 2018/10/09.
//

#ifndef ARDUINOSENSORS_SENSORHANDLER_H
#define ARDUINOSENSORS_SENSORHANDLER_H


#include "proximitySensors/Ultrasonic.h"

class SensorHandler {
public:
    SensorHandler();

private:
    static const uint8_t TOTAL_SENSOR_NUM=1;
Sensor *sensors[TOTAL_SENSOR_NUM];
public:
    void setup();
    void update();

    void report();

    Ultrasonic ultrasonic;
};


#endif //ARDUINOSENSORS_SENSORHANDLER_H
