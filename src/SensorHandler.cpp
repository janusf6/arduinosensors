//
// Created by Janus on 2018/10/09.
//

#include "SensorHandler.h"

extern uint8_t ultrasonic1TP;
extern uint8_t ultrasonic1EP;
extern uint8_t ultrasonic1Dist;

void SensorHandler::update() {
     for (int i = 0; i < TOTAL_SENSOR_NUM; ++i) {
//         Serial.println("update");
         sensors[i]->update();
     }

}

void SensorHandler::setup() {
    //add all sensors to the sensor array here.

    sensors[0] = &ultrasonic;

}

void SensorHandler::report() {
    for (int i = 0; i < TOTAL_SENSOR_NUM; ++i) {
//        Serial.println("report");
//        Serial.println( sensors[i]->isDirty());
    if (  sensors[i]->isDirty()) { ;
            Serial.print(sensors[i]->identify());
            Serial.print(i);
            Serial.print(": ");
            Serial.println(  (sensors[i]->value));
        }
    }
}

SensorHandler::SensorHandler() {
    ultrasonic=   Ultrasonic(ultrasonic1TP, ultrasonic1EP, ultrasonic1Dist);
}
