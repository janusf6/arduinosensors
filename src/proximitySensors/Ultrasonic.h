//
// Created by Janus on 2018/10/09.
//

#ifndef ARDUINOSENSORS_ULTRASONIC_H
#define ARDUINOSENSORS_ULTRASONIC_H

#include <Arduino.h>

#include <NewPing.h>
#include "../Sensor.h"

class Ultrasonic : public Sensor {
public:
//    Ultrasonic();

    Ultrasonic(uint8_t tPin_, uint8_t ePin_, int maxDist_);

    Ultrasonic();

    uint8_t triggerPin;
    uint8_t echoPin;
    int maxDist;
    NewPing sensor = NewPing(0, 0);
public:
    void update();

    int elapsed();

    unsigned long prevTime;

    void resetTimer();
        String identify();

};


#endif //ARDUINOSENSORS_ULTRASONIC_H
