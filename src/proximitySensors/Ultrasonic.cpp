//
// Created by Janus on 2018/10/09.
//

#include "Ultrasonic.h"
extern int ultrasonicPingDelay;
void Ultrasonic::update() {

//    Serial.println("update!");
    if(prevVal!=value)
    {
        prevVal=value;
        dirty=true;
    }


//    Serial.println(value);
    if(elapsed()>ultrasonicPingDelay)
    {
        value=sensor.ping_cm();
        resetTimer();
    }

}

void Ultrasonic::resetTimer() { prevTime =millis(); }

Ultrasonic::Ultrasonic(uint8_t tPin_, uint8_t ePin_, int maxDist_) {
    triggerPin=tPin_;
    echoPin=ePin_;
    maxDist=maxDist_;
    sensor=NewPing(triggerPin, echoPin, maxDist);
    resetTimer();
}



int Ultrasonic::elapsed() {
    long diff = millis() - prevTime;
    return diff;
}

String Ultrasonic::identify() {
    return "v";
}

Ultrasonic::Ultrasonic() {}

