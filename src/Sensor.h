//
// Created by Janus on 2018/10/09.
//

#ifndef ARDUINOSENSORS_SENSOR_H
#define ARDUINOSENSORS_SENSOR_H
#include <arduino.h>


class Sensor {
protected:
    unsigned int prevVal;
    bool dirty;
public:
   virtual void update()=0 ;
    virtual String   identify()=0;
    virtual bool isDirty() ;

    unsigned int value;
};


#endif //ARDUINOSENSORS_SENSOR_H
