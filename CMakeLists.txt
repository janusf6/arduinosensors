cmake_minimum_required(VERSION 3.7)

set(PROJECT_NAME arduinoSensors)
project(${PROJECT_NAME})
set(${CMAKE_PROJECT_NAME}_SKETCH ${PROJECT_NAME}.ino)
set(CMAKE_CXX_STANDARD 11)
include(includelist.cmake)
set(SOURCE_FILES src/main.cpp  src/config.h src/proximitySensors/Ultrasonic.cpp src/proximitySensors/Ultrasonic.h src/SensorHandler.cpp src/SensorHandler.h src/Sensor.cpp src/Sensor.h)
add_executable(${CMAKE_PROJECT_NAME}_x ${SOURCE_FILES} ${LIB_SRC} ${LIB_HEADERS} )